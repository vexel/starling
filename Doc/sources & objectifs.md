# Sources
    https://fr.wikipedia.org/wiki/Boids
    https://www.youtube.com/watch?v=QbUPfMXXQIY
    https://www.youtube.com/watch?v=86iQiV3-3IA
    http://www.red3d.com/cwr/boids/
    https://www.labri.fr/perso/nrougier/from-python-to-numpy/code/boid_python.py
    https://alan-turing-institute.github.io/rsd-engineeringcourse/ch01data/084Boids.html
    https://betterprogramming.pub/boids-simulating-birds-flock-behavior-in-python-9fff99375118
<src>

# Objectifs
## Modularité du nombre de dimensions
## Respect des règles de Craig W. Reynolds :
    - la cohésion : pour former un groupe, les boids se rapprochent les uns des autres ;
    - la séparation : 2 boids ne peuvent pas se trouver au même endroit au même moment ;
    - l'alignement : pour rester groupés, les boids essayent de suivre un même chemin.

## Ajouter une dynamique de leadership
    - établir un score de leadership

### Ajouter la notion de tribu/affinité entre boids (utiliser des couleurs ?)
    - dégrader pour des tribus adjacentes
    - tribus ennemies

### Établir différentes méthodes de champs de vision 
    - circulaire/sphérique
    - conique/variante 3D

