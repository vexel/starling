from numpy import *
from matplotlib import animation
from matplotlib import pyplot as plt
from IPython.display import *

boid,alert_distance,formation_flying_distance,formation_flying_strength,limits,fig,boids_count = 10,1,1000,0.125,array([1000,1750]),plt.figure(),1000
positions,axis = random.rand(2, boid) * limits[:,newaxis], plt.axes(xlim=(0, limits[0]), ylim=(0, limits[1]))
scatter = axis.scatter(positions[0, :], positions[1, :],marker='o', edgecolor='k', lw=0.5)

def square_distances(s):
    return sum(s**2,0)

def new_flock(count, lower_limits, upper_limits):
    width = upper_limits - lower_limits
    return (lower_limits[:, newaxis] + random.rand(2, count) * width[:, newaxis])

def boids_move(positions, velocities):
    #Boids veloties towards the middle
    velocities -= (positions - mean(positions, 1)[:, newaxis]) * 0.001
    separations = positions[:, newaxis, :] - positions[:, :, newaxis]
    separations_if_close = copy(separations)
    
    ## square_distances(separations) > alert_distance says if boids are far each others depending on alert distance
    separations_if_close[0, :, :][square_distances(separations) > alert_distance] = 0
    separations_if_close[1, :, :][square_distances(separations) > alert_distance] = 0
    velocities += sum(separations_if_close, 1)

    ## same logique with logic with formation_flying_distance
    vel_difs_if_close = copy(velocities[:, newaxis, :] - velocities[:, :, newaxis])
    vel_difs_if_close[0, :, :][square_distances(separations) > formation_flying_distance] = 0
    vel_difs_if_close[1, :, :][square_distances(separations) > formation_flying_distance] = 0
    velocities -= mean(vel_difs_if_close, 1) * formation_flying_strength

    positions += velocities
    
def simulation(frame):
    boids_move(positions, velocities)
    scatter.set_offsets(positions.transpose())

positions,velocities = new_flock(boids_count, array([100, 900]), array([200, 1100])),new_flock(boids_count, array([0, -20]), array([10, 20]))
animate_simulation,writergif = animation.FuncAnimation(fig, simulation,120),animation.PillowWriter(fps=60)
animate_simulation.save('example.gif',writer=writergif)
HTML(animate_simulation.to_jshtml())

