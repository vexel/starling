# Starling project / Starling 


## About / Synopsis

* A murmuration a large group of birds, usually starlings, that all fly together and change direction together
* The purpose of this project is to reproduce them in 2D and then in 3D


See real examples:

* https://github.com/djin31/Starlings/blob/master/doc/starlings.pdf
* https://www.youtube.com/watch?v=uV54oa0SyMc&t=39s

## Installation

### Languages
Using python 3.6

### Librairies
- numpy
- matplotlib  
- IPython 

## Example from this project
1000 boids examples with differents veloties towards the middle :

![example](/uploads/7780b102dde73fdaa158e263e6b05a0a/example.gif)

![example](/uploads/c907608a3eab13b0a9bfbb977247cf9b/example.gif)

